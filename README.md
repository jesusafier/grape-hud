<h1>Grape-Hud</h1>

Diablo style Health and Mana orbs module for foundryVTT.

Currently has the following features:

1. HP as health orb
2. Spell slots as mana orb
3. Shows up only for players

![](https://i.imgur.com/g3K34hm.mp4)
![](https://i.imgur.com/RhLdrKl.mp4)
<h1>Install</h1>
https://gitlab.com/jesusafier/grape-hud/-/jobs/artifacts/master/raw/module.json?job=build-module


Join the discussions on our [Discord](https://discord.gg/467HAfZ)
and if you liked it, consider supporting me on [patreon](https://www.patreon.com/foundry_grape_juice)